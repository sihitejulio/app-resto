<?php include_once 'cek_login.php';?>
<?php include 'function/connect.php'; ?>
<?php include_once 'function/admin/admin_kasir.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Interface</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Theme/css/AdminInt.css">
    <script src="Theme/js/AdminInt.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
			MENU
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				Administrator
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
			
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown ">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Account
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header">Profile</li>
							<li class="divider"></li>
							<li><a href="function/admin/admin_logout.php?logout=true">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>  	
         
    <div class="container-fluid main-container">
  		<div class="col-md-2 sidebar">
  			<div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"> </div>
            <!-- Menu -->
            <div class="side-menu">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Main Menu -->
                    <?php include "side_menu.php"; ?>
                </nav>
            </div>
        </div>  		
    </div>
  	<div class="col-md-10 content">
  		<div class="panel panel-default">
            <div class="panel-heading">
                Daftar Kasir
            </div>
        	<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="admin_add_edit_kasir.php?kasir=tambah" class="btn btn-info">Tambah Kasir</a>
                    </div>
                </div>
                <!-- admin_add_edit_kategori.php?kategori=edit&id=id -->
                <div class="row">
                    <div class="col-md-12">
                        <br>      
                          <table class="table-bordered table-condensed table-striped table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <!-- <th>Kode Produk</th> -->
                                    <th>Nama</th>
                                    <th>User Name</th>
                                    <th>level</th>
                                    <th width="18%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                    
                                <?php 
                                    $queryKasir=getAllKasir();
                                    $no=1;
                                    while($resultQueryKasir=mysql_fetch_array($queryKasir)){
                                ?>  
                                <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $resultQueryKasir['nama']; ?></td>
                                        <td><?php echo $resultQueryKasir['username']; ?></td>
                                        <td><?php echo $resultQueryKasir['level']; ?></td>
                                        <td>
                                            <a class="btn btn-info" href="admin_add_edit_kasir.php?kasir=edit&id=<?php echo $resultQueryKasir['id']; ?>">Edit</a> 
                                        </td>
                                    </tr>  
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
	        </div>
        </div>
  		</div>
  		<footer class="pull-left footer">
  			<p class="col-md-12">
  				<hr class="divider">
  				Copyright &COPY; 2015 <a href="http://www.pingpong-labs.com">Gravitano</a>
  			</p>
  		</footer>
  	</div>
</body>
</html>