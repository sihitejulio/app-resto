<?php include_once 'cek_login.php';?>
<?php include_once 'function/connect.php'; ?>
<?php include_once 'function/admin/admin_kasir.php'; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Interface</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Theme/css/AdminInt.css">
    <script src="Theme/js/AdminInt.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
			MENU
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				Administrator
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
			
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown ">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Account
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header">Profile</li>
							<li class="divider"></li>
							<li><a href="function/admin/admin_logout.php?logout=true">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>  	
         
    <div class="container-fluid main-container">
  		<div class="col-md-2 sidebar">
  			<div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"> </div>
            <!-- Menu -->
            <div class="side-menu">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Main Menu -->
                    <?php include "side_menu.php"; ?>
                </nav>
            </div>
        </div>  		
    </div>
  	<div class="col-md-10 content">
  		<div class="panel panel-default">
            <div class="panel-heading">
                <?php
                  if(isset($_GET['kasir'])){
                    if($_GET['kasir']=='tambah'){
                        echo "Tambah ";
                    }
                    if($_GET['kasir']=='edit'){
                        echo "Edit ";
                    }
                } 
                ?>Kasir
            </div>
        	<div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                    <?php
                        if($_GET['kasir']=='tambah'){
                    ?>
                        <form id="kasir_baru" class="form-horizontal" action="function/admin/admin_kasir.php" enctype="multipart/form-data" method="POST">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="nama">Nama Kasir</label>
                                <div class="col-md-5">
                                    <input id="nama" name="nama" type="text" placeholder="Nama Kasir" class="form-control input-md">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="username">Username</label>
                                <div class="col-md-5">
                                    <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="password"></label>
                                <div class="col-md-5">
                                    <input id="password" name="password" type="text" placeholder="password" class="form-control input-md">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-4">
                                    <button id="add_kasir" type="submit" name="buat_kasir" class="btn-block btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    <?php
                        }
                        if($_GET['kasir']=='edit'){
                            $id=$_GET['id'];
                            $queryKasir=getKasirWhereId($id);
                            while($resultQueryKasir=mysql_fetch_array($queryKasir)){
                                
                    ?>
                            <form id="kasir_baru" class="form-horizontal" action="function/admin/admin_kasir.php" enctype="multipart/form-data" method="POST">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="nama">Nama Kasir</label>
                                <div class="col-md-5">
                                    <input id="id" name="id" type="hidden" class="form-control input-md" value="<?php echo $resultQueryKasir['id']; ?>">
                                    <input id="nama" name="nama" type="text" placeholder="Nama Kasir" class="form-control input-md" value="<?php echo $resultQueryKasir['nama'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="username">Username</label>
                                <div class="col-md-5">
                                    <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md" value="<?php echo $resultQueryKasir['username'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="password"></label>
                                <div class="col-md-5">
                                    <input id="password" name="password" type="password" placeholder="password" class="form-control input-md" value="<?php echo $resultQueryKasir['password'];?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-4">
                                    <button id="login" type="submit" name="edit_kasir" class="btn-block btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    <?php
                            }
                        }
                    
                    ?>
                        
                    </div>
                </div>
	        </div>
        </div>
  		</div>
  		<footer class="pull-left footer">
  			<p class="col-md-12">
  				<hr class="divider">
  				Copyright &COPY; 2015 <a href="http://www.pingpong-labs.com">Gravitano</a>
  			</p>
  		</footer>
  	</div>
</body>
</html>