<?php include_once 'cek_login.php';?>
<?php include_once 'function/connect.php'; ?>
<?php include_once 'function/admin/admin_kategori_menu.php'; ?>
<?php include_once 'function/admin/admin_menu.php'; ?>
<?php include_once 'function/admin/admin_satuan_menu.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Interface</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Theme/css/AdminInt.css">
    <script src="Theme/js/AdminInt.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
			MENU
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				Administrator
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
			
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown ">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Account
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header">Profile</li>
							<li class="divider"></li>
							<li><a href="function/admin/admin_logout.php?logout=true">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>  	
         
    <div class="container-fluid main-container">
  		<div class="col-md-2 sidebar">
  			<div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"> </div>
            <!-- Menu -->
            <div class="side-menu">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Main Menu -->
                    <?php include "side_menu.php"; ?>
                </nav>
            </div>
        </div>  		
    </div>
  	<div class="col-md-10 content">
  		<div class="panel panel-default">
            <div class="panel-heading">
                <?php
                    if(isset($_GET['menu'])){
                        if($_GET['menu']=='tambah'){
                            echo "Tambah ";
                        }
                        if($_GET['menu']=='edit'){
                            echo "Edit ";
                        }
                    } 
                ?>Menu
            </div>
        	<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                         if(isset($_GET['menu'])){
                            if($_GET['menu']=='tambah'){
                    ?>
                                   <form id="produk_baru" class="form-horizontal" action="function/admin/admin_menu.php" enctype="multipart/form-data" method="POST">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="nama_menu">Nama Menu</label>
                                <div class="col-md-5">
                                    <input id="nama_menu" name="nama_menu" type="text" placeholder="Nama Menu" class="form-control input-md">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="kategori_menu">Kategori Menu</label>
                                <div class="col-md-5">
                                    <select id="kategori_menu" name="kategori_menu" class="form-control">
                                       <?php 
                                            $queryKategori=getAllKategori();
                                            while ($resultQueryKategori=mysql_fetch_array($queryKategori)){
                                        ?>  
                                            <option value="<?php echo $resultQueryKategori['id'];?>"><?php echo $resultQueryKategori['nama_kategori']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="satuan">Satuan</label>
                                <div class="col-md-5">
                                    <select id="satuan" name="satuan" class="form-control">
                                        <?php 
                                            $querySatuan=getAllSatuan();
                                            while ($resultQuerySatuan=mysql_fetch_array($querySatuan)){
                                        ?>  
                                            <option value="<?php echo $resultQuerySatuan['id'];?>"><?php echo $resultQuerySatuan['nama_satuan'];?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="kategori_menu">Harga</label>
                                <div class="col-md-5">
                                    <input id="harga_menu" name="harga_menu" type="text" placeholder="10000" class="form-control input-md">                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="deskripsi_menu">Deskripsi</label>
                                <div class="col-md-5">
                                    <textarea class="form-control" id="textarea" name="deskripsi_menu"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="img">Img</label>
                                <div class="col-md-8">
                                    <input type="file" name="img" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-4">
                                    <button id="login" type="submit" name="buat_menu_baru" class="btn-block btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>     
                    <?php
                            }
                            if($_GET['menu']=='edit'){
                                $id=$_GET['id'];
                                $queryMenu=getMenuWhereId($id);
                                while($resultQueryMenu=mysql_fetch_array($queryMenu)){
                    ?>
                        <div class="col-md-6">          
                            <form id="produk_baru" class="form-horizontal" action="function/admin/admin_menu.php" enctype="multipart/form-data" method="POST">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="nama_menu">Nama Menu</label>
                                    <div class="col-md-5">
                                        <input id="id_menu" name="id_menu" type="hidden" class="form-control input-md" value="<?php echo $resultQueryMenu['id_menu'];?>">                                
                                        <input id="nama_menu" name="nama_menu" type="text" placeholder="Nama Menu" class="form-control input-md" value="<?php echo $resultQueryMenu['nama_menu'];?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="kategori_menu">Kategori Menu</label>
                                    <div class="col-md-5">
                                        <select id="kategori_menu" name="kategori_menu" class="form-control">
                                        <?php 
                                                $queryKategori=getAllKategori();
                                                while ($resultQueryKategori=mysql_fetch_array($queryKategori)){
                                            ?>  
                                                <option value="<?php echo $resultQueryKategori['id']; ?>" <?php if($resultQueryKategori['id']==$resultQueryMenu['kategori_id']){ echo "selected='selected'"; }?>><?php echo $resultQueryKategori['nama_kategori']; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="satuan">Satuan</label>
                                    <div class="col-md-5">
                                        <select id="satuan" name="satuan" class="form-control">
                                            <?php 
                                                $querySatuan=getAllSatuan();
                                                while ($resultQuerySatuan=mysql_fetch_array($querySatuan)){
                                            ?>  
                                                <option value="<?php echo $resultQuerySatuan['id'];?>" <?php if($resultQuerySatuan['id']==$resultQueryMenu['satuan_id']){ echo "selected='selected'"; }?>><?php echo $resultQuerySatuan['nama_satuan'];?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="kategori_menu">Harga</label>
                                    <div class="col-md-5">
                                        <input id="harga_menu" name="harga_menu" type="text" placeholder="10000" value="<?php echo $resultQueryMenu['harga']; ?>" class="form-control input-md">                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="deskripsi_menu">Deskripsi</label>
                                    <div class="col-md-5">
                                        <textarea class="form-control" id="textarea" name="deskripsi_menu"> <?php echo $resultQueryMenu['deskripsi_menu']; ?></textarea>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-md-3 control-label" for="img">Img</label>
                                    <div class="col-md-8">
                                        <input type="file" name="img" class="form-control">
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="login" type="submit" name="edit_menu" class="btn-block btn btn-success">Simpan</button>
                                    </div>
                                </div>
                            </form>               
                        </div>
                        <div class="col-md-6">
                            <h3>Image Menu</h3>
                            <center>
                                <a href="#aboutModal" data-toggle="modal" data-target="#myModal">
                                <img src="menu_img/<?php echo $resultQueryMenu['img']; ?>" name="aboutme" width="140" height="140"></a>
                            </center>
                            <br>
                            <form role="Form" method="POST" action="function/admin/admin_menu.php" enctype="multipart/form-data" method="POST">
                                <div class="form-group">
                                    <input id="id_menu" name="id_menu" type="hidden" class="form-control input-md" value="<?php echo $resultQueryMenu['id_menu'];?>">
                                    <input type="file" name="img" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="change_menu_img" class="btn btn-default">Submit</button>
                                </div>
                            </form>
                        </div>
                    <?php
                                }
                            }
                        } 
                    ?>
                    </div>
                </div>
	        </div>
        </div>
  		</div>
  		<footer class="pull-left footer">
  			<p class="col-md-12">
  				<hr class="divider">
  				Copyright &COPY; 2015 <a href="http://www.pingpong-labs.com">Gravitano</a>
  			</p>
  		</footer>
  	</div>
</body>
</html>