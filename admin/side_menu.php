
<div class="side-menu-container">
    <ul class="nav navbar-nav">
        <li><a href="admin_dashboard_member.php"><span class="glyphicon glyphicon-user"></span> Member</a></li>
        <li><a href="admin_kelola_kasir.php"><span class="glyphicon glyphicon-user"></span> Kasir</a></li>
        <li><a href="admin_pesanan.php"><span class="glyphicon glyphicon-list-alt"></span> Pesanan</a></li>
        
        <!-- Dropdown-->
        <li class="panel panel-default" id="dropdown">
            <a data-toggle="collapse" href="#dropdown-lvl1">
                <span class="glyphicon glyphicon-list-alt"></span> Menu <span class="caret"></span>
            </a>

            <!-- Dropdown level 1 -->
            <div id="dropdown-lvl1" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul class="nav navbar-nav">
                        <li><a href="admin_dashboard_menu.php"><span class="glyphicon glyphicon-list-alt"></span> Daftar Menu</a></li>
                        <li><a href="admin_dashboard_kategori_menu.php"><span class="glyphicon glyphicon-tags"></span> Ketegori Menu</a></li>
                        <li><a href="admin_dashboard_satuan.php">Satuan</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a href="slide_config.php"><span class="glyphicon glyphicon glyphicon-picture"></span> Slide Config</a></li>
    </ul>
</div><!-- /.navbar-collapse -->