-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04 Jul 2018 pada 07.52
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `level` varchar(45) DEFAULT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama`, `level`, `username`, `password`) VALUES
(1, 'admin', 'super_admin', 'admin', '827ccb0eea8a706c4c34a16891f84e7b'),
(2, 'raden', 'kasir', 'raden4', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `delivery_table`
--

CREATE TABLE `delivery_table` (
  `id` int(10) NOT NULL,
  `jam_antar_pesanan` datetime DEFAULT NULL,
  `id_pesanan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dine_in_table`
--

CREATE TABLE `dine_in_table` (
  `id` int(10) NOT NULL,
  `id_pesanan` int(10) DEFAULT NULL,
  `jam_datang` datetime DEFAULT NULL,
  `jumlah_tamu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_menu`
--

CREATE TABLE `kategori_menu` (
  `id` int(2) NOT NULL,
  `nama_kategori` varchar(45) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kategori_menu`
--

INSERT INTO `kategori_menu` (`id`, `nama_kategori`, `deskripsi`) VALUES
(1, 'Makanan', 'Kategori Makananan Berat'),
(2, 'Minuman', 'Minuman'),
(3, 'Sayuran', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `id` int(10) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nama_depan` varchar(25) DEFAULT NULL,
  `nama_belakang` varchar(30) DEFAULT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `password` varchar(400) DEFAULT NULL,
  `no_virtual_account` varchar(45) DEFAULT NULL,
  `saldo` decimal(16,0) DEFAULT NULL,
  `photo` varchar(400) DEFAULT NULL,
  `status_user` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`id`, `email`, `nama_depan`, `nama_belakang`, `alamat`, `password`, `no_virtual_account`, `saldo`, `photo`, `status_user`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'joko@gmail.com', 'jokos', 'anwar', 'medan', '12345', '1231231', '207000', '2-joko@gmail.com.png', 1),
(3, 'agus@gmail.com', 'agus', 'adsadad', 'medan', '12345', '0', '0', NULL, 0),
(4, 'anwar@gmail.com', 'anwar', 'fuadi', 'medan', '12345', '0', '0', NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(10) NOT NULL,
  `nama_menu` varchar(100) DEFAULT NULL,
  `id_kategori_menu` int(2) DEFAULT NULL,
  `id_satuan` int(2) DEFAULT NULL,
  `harga` decimal(16,0) DEFAULT NULL,
  `deskripsi_menu` varchar(300) DEFAULT NULL,
  `id_status` int(2) DEFAULT NULL,
  `img` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `nama_menu`, `id_kategori_menu`, `id_satuan`, `harga`, `deskripsi_menu`, `id_status`, `img`) VALUES
(1, 'asdasd', 1, 1, '10000', 'default text', 1, NULL),
(2, 'Ayam Bakar', 1, 2, '10000', 'Ayam Bakar Dada', 1, NULL),
(3, 'Teh Manis', 1, 1, '10000', '-', 2, ''),
(4, 'Teh Manis', 1, 1, '10000', '-', 2, ''),
(5, 'Teh Manis', 1, 1, '10000', '-', 2, ''),
(6, '', 1, 1, '0', '', 2, ''),
(7, 'Teh Manis', 1, 1, '10000', '-', 1, 'Teh Manis.PNG'),
(8, 'Teh Manis', 1, 2, '10000', '-', 1, 'Teh Manis.PNG'),
(9, 'teh dingin', 1, 1, '1000', ' -', 1, 'teh dingin.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(10) NOT NULL,
  `id_pesanan` int(10) DEFAULT NULL,
  `total_bayar` decimal(16,0) DEFAULT NULL,
  `status_pembayaran` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_pesanan`, `total_bayar`, `status_pembayaran`) VALUES
(1, 0, '20000', 2),
(2, 0, '20000', 2),
(3, 6, '20000', 2),
(4, 7, '20000', 2),
(5, 8, '20000', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(10) NOT NULL,
  `id_member` varchar(10) DEFAULT NULL,
  `id_tipe_pesanan` varchar(2) DEFAULT NULL,
  `tanggal_pesan` datetime DEFAULT NULL,
  `id_status_pesanan` int(2) DEFAULT NULL,
  `id_admin` int(10) DEFAULT NULL,
  `tgl_konfirmasi` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `id_member`, `id_tipe_pesanan`, `tanggal_pesan`, `id_status_pesanan`, `id_admin`, `tgl_konfirmasi`) VALUES
(1, '2', '2', '2018-07-03 21:06:07', 1, 2, '2018-07-04 00:12:58'),
(2, '2', '2', '2018-07-03 23:24:23', 1, NULL, NULL),
(3, '2', '2', '2018-07-03 23:26:35', 1, NULL, NULL),
(4, '2', '2', '2018-07-03 23:29:17', 1, NULL, NULL),
(5, '2', '2', '2018-07-03 23:31:27', 1, NULL, NULL),
(6, '2', '2', '2018-07-03 23:32:26', 1, NULL, NULL),
(7, '2', '2', '2018-07-03 23:34:06', 2, 2, '2018-07-04 00:13:40'),
(8, '2', '2', '2018-07-03 23:38:35', 2, 2, '2018-07-04 00:22:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan_detail`
--

CREATE TABLE `pesanan_detail` (
  `id` int(10) NOT NULL,
  `jumlah` int(3) DEFAULT NULL,
  `harga` decimal(16,0) DEFAULT NULL,
  `id_pesanan` int(10) DEFAULT NULL,
  `id_menu` int(10) DEFAULT NULL,
  `catatan_tambahan` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pesanan_detail`
--

INSERT INTO `pesanan_detail` (`id`, `jumlah`, `harga`, `id_pesanan`, `id_menu`, `catatan_tambahan`) VALUES
(1, 3, '1000', 1, 9, NULL),
(2, 1, '10000', 0, 1, '123'),
(3, 1, '10000', 0, 3, ''),
(4, 1, '10000', 0, 3, ''),
(5, 1, '10000', 0, 4, ''),
(6, 1, '10000', 0, 2, ''),
(7, 1, '10000', 0, 5, ''),
(8, 1, '10000', 0, 2, ''),
(9, 1, '10000', 0, 3, ''),
(10, 1, '10000', 0, 2, ''),
(11, 1, '10000', 0, 1, ''),
(12, 1, '10000', 7, 3, ''),
(13, 1, '10000', 7, 2, ''),
(14, 1, '10000', 8, 5, ''),
(15, 1, '10000', 8, 4, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE `satuan` (
  `id` int(2) NOT NULL,
  `nama_satuan` varchar(25) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`, `deskripsi`) VALUES
(1, 'Pcs', 'Pcs'),
(2, 'Potong', 'Potong'),
(3, '', 'bungkus'),
(4, 'bungkus', 'ayam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slide_image`
--

CREATE TABLE `slide_image` (
  `id` int(11) NOT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `slide_photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `slide_image`
--

INSERT INTO `slide_image` (`id`, `image_title`, `slide_photo`) VALUES
(1, 'image 1', '1.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_buka`
--

CREATE TABLE `status_buka` (
  `jam_buka` time DEFAULT NULL,
  `jam_tutup` time DEFAULT NULL,
  `status_toko` varchar(2) DEFAULT NULL COMMENT '0 tutup, 1 buka'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_buka`
--

INSERT INTO `status_buka` (`jam_buka`, `jam_tutup`, `status_toko`) VALUES
('09:00:00', '18:00:00', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_menu`
--

CREATE TABLE `status_menu` (
  `id` int(2) NOT NULL,
  `nama_status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_menu`
--

INSERT INTO `status_menu` (`id`, `nama_status`) VALUES
(1, 'tersedia'),
(2, 'kosong');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id` int(2) NOT NULL,
  `nama_status_pembayaran` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_pembayaran`
--

INSERT INTO `status_pembayaran` (`id`, `nama_status_pembayaran`) VALUES
(1, 'belum bayar'),
(2, 'sudah bayar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_pesanan`
--

CREATE TABLE `status_pesanan` (
  `id` int(2) NOT NULL,
  `nama_status_pesanan` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_pesanan`
--

INSERT INTO `status_pesanan` (`id`, `nama_status_pesanan`) VALUES
(1, 'belum konfirmasi'),
(2, 'sudah konfirmasi'),
(3, 'kadaluarsa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_top_up`
--

CREATE TABLE `status_top_up` (
  `id` int(2) NOT NULL,
  `nama_status_top_up` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `status_top_up`
--

INSERT INTO `status_top_up` (`id`, `nama_status_top_up`) VALUES
(1, 'pending'),
(2, 'sukses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `take_away_table`
--

CREATE TABLE `take_away_table` (
  `id` int(10) NOT NULL,
  `jam_ambil_pesanan` datetime DEFAULT NULL,
  `id_pesanan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `take_away_table`
--

INSERT INTO `take_away_table` (`id`, `jam_ambil_pesanan`, `id_pesanan`) VALUES
(1, '2018-07-03 07:04:00', 1),
(2, '2018-07-03 04:24:00', 0),
(3, '2018-07-03 04:26:00', 0),
(4, '2018-07-03 04:29:00', 0),
(5, '2018-07-03 04:31:00', 0),
(6, '2018-07-03 04:32:00', 0),
(7, '2018-07-03 04:34:00', 7),
(8, '2018-07-03 04:38:00', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_pembayaran`
--

CREATE TABLE `tipe_pembayaran` (
  `id` int(2) NOT NULL,
  `nama_tipe_pembayaran` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_pesanan`
--

CREATE TABLE `tipe_pesanan` (
  `id` int(2) NOT NULL,
  `nama_tipe_pesanan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tipe_pesanan`
--

INSERT INTO `tipe_pesanan` (`id`, `nama_tipe_pesanan`) VALUES
(1, 'dine in'),
(2, 'take away'),
(3, 'delivery');

-- --------------------------------------------------------

--
-- Struktur dari tabel `top_up_saldo`
--

CREATE TABLE `top_up_saldo` (
  `id` int(11) NOT NULL,
  `no_virtual_account` varchar(45) DEFAULT NULL,
  `jumlah_top_up` decimal(16,0) DEFAULT NULL,
  `tanggal_top_up` datetime DEFAULT NULL,
  `id_status_top_up` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `top_up_saldo`
--

INSERT INTO `top_up_saldo` (`id`, `no_virtual_account`, `jumlah_top_up`, `tanggal_top_up`, `id_status_top_up`) VALUES
(1, '123123150000', '50000', '2018-07-04 05:58:18', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transfer_virtual_account`
--

CREATE TABLE `transfer_virtual_account` (
  `id` int(11) NOT NULL,
  `no_va` varchar(45) DEFAULT NULL,
  `nominal` decimal(16,0) DEFAULT NULL,
  `tgl_transfer` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `transfer_virtual_account`
--

INSERT INTO `transfer_virtual_account` (`id`, `no_va`, `nominal`, `tgl_transfer`) VALUES
(1, '1231231', '10000', '2018-06-18 22:26:08'),
(2, '1231231', '10000', '2018-06-23 03:32:55'),
(3, '1231231', '10000', '2018-06-23 03:38:34'),
(4, '1231231', '50000', '2018-07-04 11:02:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_table`
--
ALTER TABLE `delivery_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dine_in_table`
--
ALTER TABLE `dine_in_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_menu`
--
ALTER TABLE `kategori_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan_detail`
--
ALTER TABLE `pesanan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_image`
--
ALTER TABLE `slide_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_menu`
--
ALTER TABLE `status_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pesanan`
--
ALTER TABLE `status_pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_top_up`
--
ALTER TABLE `status_top_up`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `take_away_table`
--
ALTER TABLE `take_away_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_pembayaran`
--
ALTER TABLE `tipe_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_pesanan`
--
ALTER TABLE `tipe_pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_up_saldo`
--
ALTER TABLE `top_up_saldo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_virtual_account`
--
ALTER TABLE `transfer_virtual_account`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `delivery_table`
--
ALTER TABLE `delivery_table`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dine_in_table`
--
ALTER TABLE `dine_in_table`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori_menu`
--
ALTER TABLE `kategori_menu`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pesanan_detail`
--
ALTER TABLE `pesanan_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `slide_image`
--
ALTER TABLE `slide_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status_menu`
--
ALTER TABLE `status_menu`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status_pesanan`
--
ALTER TABLE `status_pesanan`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status_top_up`
--
ALTER TABLE `status_top_up`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `take_away_table`
--
ALTER TABLE `take_away_table`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tipe_pesanan`
--
ALTER TABLE `tipe_pesanan`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `top_up_saldo`
--
ALTER TABLE `top_up_saldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transfer_virtual_account`
--
ALTER TABLE `transfer_virtual_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
