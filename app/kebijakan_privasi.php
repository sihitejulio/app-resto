<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>
<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <br>
	<div class="well">
		<h2 class="text-divider"><span>Kebijakan dan Privasi</span></h2>
		<ol>
            <h4>RUANG LINGKUP DAN PERSETUJUAN TERHADAP KETENTUAN PRIVASI</h4>
            <p>Ayam Penyet Sidoharjo berkomitmen untuk melindungi privasi Anda dan memastikan bahwa Anda dapat mempercayai Ayam Penyet Sidoharjo dengan informasi pribadi yang anda berikan dalam situs ini.</p>
            <p>Dengan menggunakan situs web Ayam Penyet Sidoharjo, atau aplikasi-aplikasi di jaringan sosial pihak ketiga (misalnya Facebook, Instagram, Twitter) atau dengan memberikan kami data pribadi Anda, Anda telah menerima praktik-praktik yang dijelaskan dalam Pernyataan Privasi ini.</p>
            <p>Penyerahan data pribadi Anda melaui Situs ini oleh Anda menandakan bahwa Anda telah menyetujui semua persyaratan dari Ketentuan Privasi ini. Jadi mohon untuk tidak menyerahkan data pribadi apapun melalui Situs ini jika Anda tidak setuju dengan sebagian atau keseluruhan isi dari Ketentuan Privasi ini.</p>
            <p>Ayam Penyet Sidoharjo memiliki hak untuk membuat perubahan pada Pernyatan Privasi ini kapan pun. Kami mengimbau Anda untuk membaca Pernyataan Privasi ini secara berkala untuk memastikan bahwa Anda mengetahui perubahaan apa pun yang berlaku dan bagaimana data pribadi Anda digunakan.</p>
            <p>Kami percaya bahwa penting bagi anda untuk mengetahui bagaimana kami memperlakukan informasi pribadi anda. Segala ketentuan dalam kebijakan privasi ini berlaku untuk semua pengguna situs ini. Di bawah ini akan anda temukan jawaban dari berbagai pertanyaan yang berkaitan dengan kebijakan privasi yang telah diperbarui ini.</p>
        </ol>
        <ol>
            <h4>Penggunaan informasi yang terkumpul</h4>
            <ul>
                <li>
                    memahami penggunaan situs dan membuat perbaikan
                </li>
                <li>
                    memberi respon atas permintaan spesifik dari pengunjung
                </li>
                <li>
                    mendaftarkan pengunjung untuk aktivitas online seperti: kontes, survei, formulir komentar, atau kegiatan online interaktif lainnya
                </li>
                <li>
                    melindungi keamanan dan integritas situs jika diperlukan
                </li>
                <li>
                    mengirimkan pemberitahuan tentang promosi, penawaran, atau permintaan khusus melalui metode yang sudah anda pilih
                </li>
                <li>
                    secara umum, mempromosikan dan memasarkan bisnis dan berbagai produk Ayam Penyet Sidoharjo.
                </li>
            </ul>
        </ol>
	</div>
</div>
<?php include "footer.php"; ?>
</body>
</html>