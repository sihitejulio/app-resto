<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>

</head>
<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

<body>   
<?php include "navigation_user.php"; ?>
<div class="container">
    <br>
	<div class="well">
		<h2 class="text-divider"><span>Tentang Ayam Penyet Sidoharjo</span></h2>
		<ol>
            <h4>PERSYARATAN LAYANAN AYAM PENYET SIDOHARJO</h4>
            <p>Terima kasih Anda telah menggunakan produk dan layanan kami. Layanan disediakan oleh Ayam Penyet Sidoharjo.</p>
                <p> Silahkan baca persyaratan ini dengan saksama. Jika Anda mengakses situs kami atau memulai transaksi di situs kami, Anda kami anggap telah membaca, memahami dan menyetujui persyaratan dan ketentuan yang berlaku.</p>
                <p> Persyaratan ini berlaku untuk semua pengunjung, pengguna langsung, dan pengguna lain yang mengakses atau menggunakan Layanan kami.</p>
                <p> Layanan kami akan selalu berkembang sehingga terkadang persyaratan tambahan atau persyaratan produk dapat berlaku. Persyaratan tambahan tersedia pada Layanan yang relevan, dan persyaratan tambahan tersebut menjadi bagian dari perjanjian Anda dengan kami jika Anda menggunakan Layanan tersebut.</p>
        </ol>
        <ol>
            <h4>PERSYARATAN PEMESANAN</h4>
            <p>Saat Anda membeli produk atau layanan apa pun yang tersedia melalui Layanan kami, Anda mungkin akan diminta untuk memberikan informasi tertentu (misalnya nama, alamat e-mail, alamat, no. telepon) yang memungkinkan kami untuk mengidentifikasi Anda sebagai individu. Kami berkomitmen melindungi privasi Anda dan memastikan bahwa informasi pribadi Anda terlindungi.</p>
            <p>Jam operasional kami bisa di liat di <a href="JamOp.html">Jam Operasional</a>.</p>
            <p>Untuk pemesanan pengiriman kami hanya menerima pembayaran Virtual Account.</p>
            <p>Pesanan tidak dapat di rubah atau dibatalkan dan tidak ada pengembalian uang setelah pesanan di konfirmasi kepada Anda.</p>
            <p>Untuk menjaga keselamatan dan keamanan karyawan kami mungkin Layanan pengiriman tidak tersedia untuk sementara waktu di area karena cuaca buruk atau keadaan lainnya yang tidak terkendali.</p>
            <p>Ayam Penyet Sidoharjo berhak untuk membatalkan dan memblokir akses Anda terhadap Layanan kami apabila terindikasi Anda melakukan pelanggaran syarat dan ketentuan yang berlaku.</p>
            <p>Jika Anda memiliki pertanyaan atau komentar apapun terkait dengan persyaratan Layanan kami, silahkan <a href="HubKami.html">hubungi kami</a> secara online melalui website.</p>
        </ol>
	</div>
</div>

    <?php include "footer.php"; ?>

</body>
</html>