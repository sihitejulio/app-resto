<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php";?>
    
</head>

<body>
<?php include_once "navigation_user.php"; ?>    
<div class="container">
    <br>
	<div class="well">
		<h2 class="text-divider"><span>Top Up</span></h2>
		
    </div>
    <div class="col-md-5 col-md-offset-4">
        <form id="form_top_up" method="post" action="javascript::">
            <div class="form-group">
                <input type="hidden" name="top_up" value="ok">
                <label for="sel1">Pilih Nominal Saldo</label>
                <select class="form-control" id="nominal" name="nominal">
                    <!-- <option value="10000">Rp 10.000</option> -->
                    <option value="20000">Rp 20.000</option>
                    <option value="50000">Rp 50.000</option>
                    <option value="10000">Rp 100.000</option>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control btn btn-default btn-success" name="top_up" type="submit" value="Top Up">
            </div>
        </form>
    </div>
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="modalTopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Top UP Info</h5>
            </div>
            <div class="modal-body">
                <div id="info_top_up">

                </div>
            </div>
        </div>
    </div>
</div>

    <footer>
        <div class="container">
            <div class="row text-center">

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="menu">
                        <span>Tentang APS</span>
                        <li>
                            <a href="ProAPS.html">Profile APS</a>
                        </li>

                        <li>
                            <a href="LokOut.html">Lokasi Outlet</a>
                        </li>

                        <li>
                            <a href="Abous.html">About Us</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="menu">
                        <span>Customer Service</span>
                        <li>
                            <a href="JamOP.html">Jam Operasional</a>
                        </li>

                        <li>
                            <a href="HubKami.html">Hubungi Kami</a>
                        </li>

                        <li>
                            <a href="SarKom.html">Saran dan Komentar</a>
                        </li>
                        <li>
                            <a href="FAQ.html">FAQ</a>
                        </li>
                    </ul>
                </div>



                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="adress">
                        <span>Policies</span>
                        <li>
                            <a href="KebPri.html">Kebijakan Privasi</a>
                        </li>

                        <li>
                            <a href="SyaKet.html">Syarat dan Ketentuan</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-lg-4 col-lg-4 col-xs-12" id="icons">
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-tumblr"></i></a>
                    </div>
                </div>


            </div>
        </div>
    </footer>

    <!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="js.js"></script> -->

    <?php include_once "footer.php";?>
    
</body>
</html>