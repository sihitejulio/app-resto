<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "head.php"; ?>
	<!-- <link rel="stylesheet" href="Theme/css/Index.css"> -->
    <link rel="stylesheet" href="Theme/css/Invoice.css">
	
</head>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Bukti Pemesanan Dine In</h2><h3 class="pull-right">Order # 12345</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Ditagihkan Kepada:</strong><br>
    					Merkar Sari<br>
    					082182737576<br>
    					144042<br>
    					Jl. Karya Wisata Perum Citra Wisata Blok.11 No.16
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<strong></strong><br>
    					<br>
    					<br>
    					<br>
    					<br>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Pembayaran Dengan:</strong><br>
    					Virtual Account<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Tanggal Order:</strong><br>
    					28 April 2018<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Pesanan</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Tambahan</strong></td>
        							<td class="text-center"><strong>Harga</strong></td>
        							<td class="text-center"><strong>Jumlah</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<tr>
    								<td>Ayam Penyet + Nasi</td>
    								<td class="text-center"></td>
    								<td class="text-center">Rp. 18.000,-</td>
    								<td class="text-center">1</td>
    								<td class="text-right">Rp. 18.000,-</td>
    							</tr>
                                <tr>
        							<td>Nasi Goreng Biasa</td>
        							<td class="text-center"></td>
    								<td class="text-center">Rp. 10.000,-</td>
    								<td class="text-center">10</td>
    								<td class="text-right">Rp. 100.0000,-</td>
    							</tr>
                                <tr>
            						<td>Nasi Goreng Spesial</td>
            						<td class="text-center"></td>
    								<td class="text-center">Rp. 22.000,-</td>
    								<td class="text-center">1</td>
    								<td class="text-right">Rp. 22.000,-</td>
    							</tr>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right">Rp. 140.000,-</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Shipping</strong></td>
    								<td class="no-line text-right">Rp. 10.000,-</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">Rp. 150.000,-</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

<?php include "footer.php"; ?>

</body>
</html>