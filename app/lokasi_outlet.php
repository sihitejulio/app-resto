<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>        
    <script src="Theme/js/LokOut.js"></script>
</head>
<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <br>
	<div class="well">
		<h2 class="text-divider"><span>Lokasi Ayam Penyet Sidoharjo</span></h2>
		<div id="googleMap" style="width:100%;height:400px;"></div>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkOKg78f-ReYWyFAk9kOm15P2SKOSWQVE&callback=myMap"></script>
	</div>
</div>
<?php include "footer.php"; ?>
</body>
</html>