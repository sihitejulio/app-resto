<footer>
        <div class="container">
            <div class="row text-center">

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="menu">
                        <span>Tentang APS</span>
                        <li>
                            <a href="profile_pas.php">Profile APS</a>
                        </li>

                        <li>
                            <a href="lokasi_outlet.php">Lokasi Outlet</a>
                        </li>

                        <li>
                            <a href="about_us.php">About Us</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="menu">
                        <span>Customer Service</span>
                        <li>
                            <a href="jam_operasional.php">Jam Operasional</a>
                        </li>

                        <li>
                            <a href="hubungi_kami.php">Hubungi Kami</a>
                        </li>

                        <li>
                            <!-- <a href="SarKom.html">Saran dan Komentar</a> -->
                        </li>
                        <li>
                            <a href="faq.php">FAQ</a>
                        </li>
                    </ul>
                </div>



                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="adress">
                        <span>Policies</span>
                        <li>
                            <a href="kebijakan_privasi.php">Kebijakan Privasi</a>
                        </li>

                        <li>
                            <a href="syarat_ketentuan.php">Syarat dan Ketentuan</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-lg-4 col-lg-4 col-xs-12" id="icons">
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-tumblr"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>



<?php
      if (session_status() == PHP_SESSION_NONE) {
            session_start();
      }else{
            $sesi_user= isset($_SESSION['member']) ? $_SESSION['member'] : NULL;
            if ($sesi_user != NULL || !empty($sesi_user))
            {
?>
                  <script>            
                        var no_va='<?php echo $_SESSION['member']['no_virtual_account']; ?>';
                        var nama ='<?php echo $_SESSION['member']['nama_depan'].' '.$_SESSION['member']['nama_belakang']; ?>';
                  
                 </script>
<?php
            }
      }
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="Theme/js/Home.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


<script src="Theme/js/resto.js"></script>
<script src="loadingOverlay.js"></script>

