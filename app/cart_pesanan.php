<?php include_once 'function/connect.php';    ?>

<?php include "function/member/member_menu.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php";?>
</head>

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
  <div class="row">
    <div class="well">
        PESANAN SAYA
    </div>
  </div>  
   
   
    <div class="row" id="cart_content">
    <?php
       if(!empty($_SESSION["cart_item"]) AND count($_SESSION["cart_item"])>0){
        $total_bayar=0;
        foreach ($_SESSION["cart_item"] as $item){  
            $id_menu= substr($item['id'],5, strlen($item['id']));
            $total_bayar+=($item['jumlah']*$item['harga']);
            $sqlMenu=getMenuId($id_menu);
            while ($queryMenu=mysql_fetch_array($sqlMenu)) {
    ?>
        <div id="<?php echo $item['id']; ?>" class="col-md-12" style="background-color:white;padding:10px;">
            <div class="col-md-2">
                <?php 
                    if($queryMenu['img']==null||$queryMenu['img']==''){
                ?>
                    <img src="http://placehold.it/320x320" alt="Alternate Text" class="img-responsive" />
                <?php
                    }else{
                ?>
                    <img src="../admin/menu_img/<?php echo $queryMenu['img']; ?>" name="<?php echo $queryMenu['nama_menu']; ?>" width="220" height="120">
                <?php
                    }
                ?>
            </div>
            <div class="col-md-10">
                <div class="team-content" style="padding:5px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <span style="font-weight:600;font-size:16px;"> <?php echo $queryMenu['nama_menu']; ?></span>
                                <div class="border-team"></div>
                                <p>Rp. <?php echo number_format($queryMenu['harga'],0,',','.'); ?></p>
                            </div>
                            <div class="col-md-6 pull-right">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button   type="button" class="quantity-left-minus btn btn-default btn-number"  data-type="minus" data-field="" onclick="kurang_jumlah(<?php echo $queryMenu['id']?>,'1')">
                                                <span class="glyphicon glyphicon-minus"></span>
                                                </button>
                                            </span>
                                            <input  type="text" id="jumlah_item_menu<?php echo $queryMenu['id']; ?>" name="jumlah_item_menu" class="form-control input-number text-center" value="<?php echo $item['jumlah']; ?>" min="1" max="100">
                                            <span  class="input-group-btn" >
                                                <button    type="button" class="quantity-right-plus btn btn-default btn-number" data-type="plus" data-field="" onclick="tambah_jumlah(<?php echo $queryMenu['id']?>,'1')">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-top:8px;">
                                        <span style="font-weight:600;font-size:14px;">Total : </span><span style="font-weight:600;font-size:14px;" id="total_harga<?php echo $queryMenu['id'];?>">Rp . <?php echo number_format($queryMenu['harga']*$item['jumlah'],0,',','.'); ?> </span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="padding:5px;margin-left:15px;">
                                            <label>Catatan Tambahan</label>
                                            <textarea class="form-control" id="catatan_<?php echo $item['id']; ?>" cols="30" rows="2" onkeyup="addCatatan('<?php echo $item['id']; ?>')"><?php if(isset($item['catatan'])){ echo $item['catatan']; }?></textarea>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <hr> -->
        </div>
    <?php       } 
            }
    ?>
         <div id="checkout_part" class="col-md-12 " style="background-color:white;padding:10px;">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="col-md-12">
                    <div class="col-md-5"></div>
                    <div class="col-md-7 pull-right">
                            <div class="col-md-4"></div>
                            <div class="col-md-8" style="padding-top:8px;">
                                <span style="font-weight:600;font-size:16px;">Total Bayar : </span><span style="font-weight:600;font-size:16px;" id="total_bayar">Rp . <?php echo number_format($total_bayar,0,',','.'); ?> </span>
                                <br>
                                
                                <label class="radio-inline">
                                    <input type="radio" data-toggle="modal" data-target="#dine_in_modal" name="tipe_pesanan" id="dine_in" value="1"><span style="font-weight:600;font-size:16px;">Dine In</span> 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" data-toggle="modal" data-target="#take_away_modal" name="tipe_pesanan" id="take_away" value="2"><span style="font-weight:600;font-size:16px;">Take Away</span>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" data-toggle="modal" data-target="#delivery_in_modal" name="tipe_pesanan" id="delivery" value="3"><span style="font-weight:600;font-size:16px;">Delivery</span> 
                                </label>
                                <div>
                                    <div class="form-group">                            
                                        <input type="text" class="form-control" name="delivery" id="order_delivery" style="display:none">
                                    </div>
                                    <div class="form-group">                            
                                        <input type="text" class="form-control" name="dine-in" id="dine-in" style="display:none">
                                        <label id="label-tamu" style="display:none">Tamu</label>
                                        <input type="text" class="form-control" name="tamu" id="tamu" style="display:none">
                                    </div>
                                    <div class="form-group">                                
                                        <input type="text" class="form-control" name="take-away" id="take-away" style="display:none">
                                    </div>
                                </div>

                                <div class="modal fade" id="dine_in_modal" role="dialog">
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Jam Datang</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class='input-group date' id='datetimepicker3'>
                                                    <input type='text' id='pilih_jam_in' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" onclick="pilih_jam_datang()">Pilih</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="take_away_modal" role="dialog">
                                    <div class="modal-dialog">
                                    
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Jam Ambil</h4>
                                            </div>    
                                            <div class="modal-body">
                                                <div class='input-group date' id='datetimepicker4'>
                                                    <input type='text' id='pilih_jam_take' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" onclick="pilih_jam_ambil()">Pilih</button>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="delivery_in_modal" role="dialog">
                                    <div class="modal-dialog">
                                    
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Jam Antar</h4>
                                            </div>    
                                            <div class="modal-body">
                                                <div class='input-group date' id='datetimepicker5'>
                                                    <input type='text' id='pilih_jam_antar' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" onclick="pilih_jam_antar()">Pilih</button>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <button id='bayar' class="col-md-12 btn btn-default btn-sm" style="color:white;background-color: #4caf50 !important;"  onclick="bayar()">Bayar</button>                          
                            </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
        }
    ?>
    </div>            
</div>
    <?php include_once "footer.php";?>
</body>
</html>