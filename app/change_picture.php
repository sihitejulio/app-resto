<?php
    include_once 'function/connect.php';        
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Change Password</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>

<body>
<?php include "navigation_user.php"; ?>
<?php include_once "function/member/member_profile.php"; ?>


    <div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
			<div class="tab-content">
				<div id="changepass" class="tab-pane fade  fade in active">
                    <center>
                        <a href="#aboutModal" data-toggle="modal" data-target="#myModal">
                           <?php
                           
                                    $r       = mysql_fetch_array($sql_user);
                           ?>
                            <img src="Photo/user_<?php echo $r['id']?>/<?php echo $r['photo']?>" name="aboutme" width="140" height="140" class="img-circle"></a>
                            
                        <h3>Image Profile</h3>
                    </center>
					<form role="Form" method="POST" action="function/member/member_change_profile.php" enctype="multipart/form-data">
						
						<div class="form-group">
							<input type="file" name="file_photo" class="form-control">
						</div>
						<div class="form-group">
							<button type="submit" name="change_profile" class="btn btn-default">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
    <?php include "footer.php"; ?>
</body>
</html>