filterSelection("all") // Execute the function and show all columns
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("column");
  if (c == "all") c = "";
  // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

// Show filtered elements
function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

// Hide elements that are not selected
function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1); 
    }
  }
  element.className = arr1.join(" ");
}

// Add active class to the current button (highlight it)
// var btnContainer = document.getElementById("myBtnContainer");
// var btns = btnContainer.getElementsByClassName("btn");
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function(){
//     var current = document.getElementsByClassName("active");
//     current[0].className = current[0].className.replace(" active", "");
//     this.className += " active";
//   });
// }


function addToCart(id,jlh){
  
  $.ajax({
    type: 'post',
    url: 'function/member/member_cart.php',
    data: {id:id,jumlah:jlh,add_cart:'ok'},
    success: function (data) {

      if(data!='dont_login'){
        $('#jumlah_item_menu'+id).val(jlh);
        $('#btnAddCart'+id).hide();
        $('#beforeAddCart'+id).show();
      }else{
        alert('Silahkan Login Terlebih Dahulu');
      }
    }
  });
}

function kurang_jumlah(id,jlh){
  $.ajax({
    type: 'post',
    url: 'function/member/member_cart.php',
    data: {id:id,jumlah:jlh,kurang:'ok'},
    success: function (data) {
      var current=parseInt($('#jumlah_item_menu'+id).val());
      $('#jumlah_item_menu'+id).val(current-parseInt(jlh));    
      var json_data= $.parseJSON(data);
      if(json_data['jumlah_item']==0){
        $("#"+json_data['item']).remove();
      }
      if(json_data['jumlah_cart']=0){
        $("#checkout_part").remove();
        console.log(json_data);
        // $('#checkout_part').html('<div class="col-md-12 text-center"><h3>Cart Kosong</h3></div> ');
      }
      $('.cart-btn').html(json_data['jumlah']);
      $('#total_harga'+id).html(json_data['nominal']);
      $('#total_bayar').html(json_data['total_bayar']);
    }
  });
}

function tambah_jumlah(id,jlh){
  $.ajax({
    type: 'post',
    url: 'function/member/member_cart.php',
    data: {id:id,jumlah:jlh,tambah:'ok'},
    success: function (data) {
      var current=parseInt($('#jumlah_item_menu'+id).val());
      $('#jumlah_item_menu'+id).val(current+parseInt(jlh));  
      var json_data= $.parseJSON(data);
      console.log(json_data);
      $('.cart-btn').html(json_data['jumlah']);
      $('#total_harga'+id).html(json_data['nominal']);
      $('#total_bayar').html(json_data['total_bayar']);

    }
  });
}

function addCatatan(id){
   
  var message = $('#catatan_'+id).val(); 
  // alert(message);

  $.ajax({
    type: 'post',
    url: 'function/member/member_cart.php',
    data: {id:id,message:message,catatan:'ok'},
    success: function (data) {
     
    }
  });
}

function top_up(){
    alert('Silahkan login untuk melakukan topup');
  }
// });

function bayar(){
  if($('input[name=tipe_pesanan]').is(':checked')) { 
    var tipe_pesanan=$('input[name=tipe_pesanan]:checked').val();
    var dine=$('#dine-in').val();
    var take=$('#take-away').val();
    var order_delivery=$('#order_delivery').val();
    var tamu=$('#tamu').val();

    $.ajax({
      type: 'post',
      url: 'function/member/member_cart.php',
      data: {bayar:'ok',tipe_pesanan:tipe_pesanan,dine:dine,order_delivery:order_delivery,tamu:tamu},
      success: function (data) {
        // var current=parseInt($('#jumlah_item_menu'+id).val());
        if(JSON.parse(data).saldo==0 ||JSON.parse(data).saldo=='tidak_cukup' ){
          alert('Saldo Anda Tidak Cukup');
        }else{
          // console.log(data);
          $("#cart_content").remove();
        }
      }
    });
  }else{
    alert("Tipe Pesanan Belum Dipilih"); 
  }
}

$(function () {
  $('#form_top_up').on('submit', function(e) {
    // Prevent form submission by the browser
    var formData=$('#form_top_up').serialize();
    e.preventDefault();

    var nominal=$('#nominal').val();
    // console.log(formData().nominal);
    $.ajax({
        type: 'post',
        url: 'function/member/member_top_up.php',
        data: formData,
        beforeSend : function(){
            var spinHandle = loadingOverlay.activate();
            setTimeout(function() {
                loadingOverlay.cancel(spinHandle);
            }, 500);
            console.log('a');
        },
        success: function (data) {
            if(data=='true'){
                console.log(nominal);
                // jQuery.noConflict();
                var info ='<center> <h3>Lakukan Pembayaran Top Up saldo</h3>';
                    info +='<h4>'+nominal+'</h4>';
                    info +='<h3>Ke VA'+no_va+'</h3>';
                    info +='<h4>an '+nama+'</h4>';
                    info += '</center>';
                $('#info_top_up').html(info);
                $('#modalTopUp').modal('show'); 
            }
            // alert(value);
        }
    });
  });
});


$(function () {
  $('#datetimepicker3').datetimepicker({
      format: 'HH:mm'
  });
  $('#datetimepicker4').datetimepicker({
    format: 'HH:mm'
  });
  $('#datetimepicker5').datetimepicker({
    format: 'HH:mm'
  });
});

function pilih_jam_datang()
{
  $('#dine-in').show();
  $('#label-tamu').show();
  $('#tamu').show();
  $('#take-away').hide();
  $('#dine-in').val(  
    $('#pilih_jam_in').val()
  );
  
  $('#take-away').val('');
  $('#order_delivery').val('');
}

function pilih_jam_ambil(){
  $('#dine-in').hide();
  $('#label-tamu').hide();
  $('#tamu').hide();
  $('#take-away').show();
  $('#take-away').val($('#pilih_jam_take').val());
  $('#dine-in').val('');
  $('#order_delivery').val('');
}


function pilih_jam_antar(){
  // console.log('sdad');
  $('#label-tamu').hide();
  $('#tamu').hide();
  $('#order_delivery').show();
  $('#dine-in').hide();
  $('#take-away').hide();
  $('#order_delivery').val($('#pilih_jam_antar').val());
  $('#dine-in').val('');
  $('#take-away').val('');  
}