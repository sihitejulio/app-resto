function myMap() {
  var myCenter = new google.maps.LatLng(3.541262, 98.654427);
      var mapCanvas = document.getElementById("googleMap");
      var mapOptions = {center: myCenter, zoom: 20};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);
}