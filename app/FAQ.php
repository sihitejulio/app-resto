<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>
<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <br>
	<div class="well">
		<h2 class="text-divider"><span>FAQ</span></h2>
		<ol>
            <li>
                <p class="question">Dimana Lokasi Penjualan Ayam Penyet Sidoharjo Berada ?</p>
                <div class="answer">
                    <p>Penjualan Ayam Penyet Sidoharjo Berjualan di lokasi Medan Sekitar</p>
                </div>
            </li>
            <li>
                <p class="question">Bagaimana cara memesan Ayam Penyet Sidoharjo ?</p>
                <div class="answer">
                    <p>Pemesanan Ayam Penyet Sidoharjo dapat dilakukan dengan cara takeaway, dine in atau delivery yang bisa dilakukan lewat pesan online dengan website ataupun telepon.</p>
                </div>
            </li>
            <li>
                <p class="question">Bagaimana cara melakukan pembayaran di Ayam Penyet Sidoharjo?</p>
                <div class="answer">
                    <p>Pembayaran dilakukan dengan Menggunakan Virtual Account</p>
                </div>
            </li>
            <li>
                <p class="question">Pembayaran Online</p>
                <div class="answer">
                    <p>Delivery</p>
                    <p>Melalui sistem pembayaran online ini customer dapat mengirimkan pesanan kepada keluarga, teman atau relasi bisnis ke alamat yg berbeda.</p>
                    <p>Takeaway</p>
                    <p>Melalui sistem pembayaran online takeaway customer tak perlu menunggu lama di Outlet Ayam Penyet Sidoharjo. Dengan melakukan pemesanan terlebih dahulu dari rumah atau kantor, pesanan akan langsung tersedia, ketika customer tiba di outlet Ayam Penyet Sidoharjo.</p>
                    <p>Dine In</p>
                    <p>Melalui sistem pembayaran online ini customer dapat memesan makanan terlebih dahulu dari rumah dan pada saat datang bisa langsung makan di tempat.</p>
                </div>
            </li>
        </ol>
	</div>
</div>
    <?php include "footer.php"; ?>
</body>
</html>