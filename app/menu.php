<?php include_once 'function/connect.php';    ?>
<?php include "function/member/member_menu.php"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php";?>
</head>

<body>
<?php include "navigation_user.php"; ?>
<div class="container">
   <h2>Menu</h2>
    <div id="myBtnContainer">
        <?php
            $sqlKategori=getKategori();
            while ($queryKategori=mysql_fetch_array($sqlKategori)) {
        ?>                
                <button class="btn" onclick="filterSelection('<?php echo $queryKategori['nama_kategori']; ?>')"> <?php echo $queryKategori['nama_kategori']; ?></button>
        <?php
            }
        ?>
    </div>
   
    <div class="row">
    <?php
        $sqlMenu=getMenu();
        while ($queryMenu=mysql_fetch_array($sqlMenu)) {
            // print_r($queryMenu);
                   
                    // $sqlMenuCart=getMenuId($item['id']);
                    // while ($queryMenucart=mysql_fetch_array($sqlMenuCart)) {
                       
                            // echo "sdad";
                      
    ?>
        <div class="col-md-4 column <?php echo $queryMenu['nama_kategori']; ?>">
            <div class="team-img thumbnail">
                <?php 
                    // $r=mysql_fetch_array($sql_user);
                    if($queryMenu['img']==null||$queryMenu['img']==''){
                ?>
                    <img src="http://placehold.it/320x320" alt="Alternate Text" class="img-responsive" />
                <?php
                    }else{
                ?>
                    <img src="../admin/menu_img/<?php echo $queryMenu['img']; ?>" name="<?php echo $queryMenu['nama_menu']; ?>" width="320" height="320">
                <?php
                    }
                ?>
                <div class="team-content" style="padding:5px; height:100px;">
                    <div class="row">
                        <div class="col-md-6">
                            <span style="font-weight:600;font-size:16px;"> <?php echo $queryMenu['nama_menu']; ?></span>
                            <div class="border-team"></div>
                            <p>Rp. <?php echo number_format($queryMenu['harga'],0,',','.'); ?></p>
                        </div>
                        <div class="col-md-4 pull-right">
                           <?php
                            //   if(!empty($_SESSION["cart_item"])){
                                if(isset($_SESSION["cart_item"]['menu_'.$queryMenu['menu_id']])){
                                    $item=$_SESSION["cart_item"]['menu_'.$queryMenu['menu_id']];
                           ?>
                                <div class="input-group" style="margin-left:-28px;padding-left:0px;">
                                    <span class="input-group-btn">
                                        <button   type="button" class="quantity-left-minus btn btn-default btn-number"  data-type="minus" data-field="" onclick="kurang_jumlah(<?php echo $queryMenu['menu_id']?>,'1')">
                                        <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="text" id="jumlah_item_menu<?php echo $queryMenu['menu_id']?>" name="jumlah_item_menu" class="form-control input-number" value="<?php echo $item['jumlah']; ?>" min="1" max="100">
                                    <span class="input-group-btn">
                                        <button   type="button" class="quantity-right-plus btn btn-default btn-number" data-type="plus" data-field="" onclick="tambah_jumlah(<?php echo $queryMenu['menu_id']?>,'1')">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                          <?php
                                // }
                            }else{
                                if($queryMenu['id_status']!='2'){
                           ?>
                              <button id='btnAddCart<?php echo $queryMenu['menu_id']?>' class="btn btn-default btn-sm" style="color:white;background-color: #4caf50 !important;"  onclick="addToCart('<?php echo $queryMenu['menu_id']?>','1')">Tambah</button>                          
                              <div id='beforeAddCart<?php echo $queryMenu['menu_id']?>'  class="input-group" style="margin-left:-22px;display:none;">
                                <span class="input-group-btn">
                                    <button   type="button" class="quantity-left-minus btn btn-default btn-number"  data-type="minus" data-field="" onclick="kurang_jumlah(<?php echo $queryMenu['menu_id']?>,'1')">
                                    <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" id="jumlah_item_menu<?php echo $queryMenu['menu_id']?>" name="jumlah_item_menu" class="form-control input-number" value="" min="1" max="100">
                                <span class="input-group-btn">
                                    <button   type="button" class="quantity-right-plus btn btn-default btn-number" data-type="plus" data-field="" onclick="tambah_jumlah(<?php echo $queryMenu['menu_id']?>,'1')">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>  
                           
                            <?php  
                                }else{
                            ?>
                              <button  class="btn btn-danger btn-sm">Habis</button>                          
                               
                            <?php
                                }   
                            }
                          ?>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php                 
        }
    ?>
    </div>            
</div>

    <?php include_once "footer.php";?>

</body>
</html>