<!DOCTYPE html>
<html lang="en">
<head>
    <title>About Us</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>
<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <br>
    <div class="well">
        <h2 class="text-divider"><span>About Us</span></h2>

        <div class="aboutus-secktion paddingTB60">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="strong">Siapa Kami dan<br>Apa yang kami lakukan</h1>
                        <p class="lead">Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit.</p>
                    </div>
                    <div class="col-md-5">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam. Lorem ipsum dolor sit amet. Nulla convallis egestas rhoncus.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-heading text-center">
                    <h3>Team Kami</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua. Ut enim ad minim </p>
                    <div class="border"></div>
                </div>
        <div class="row">
                <div class="col-md-4 team-box">
                    <div class="team-img thumbnail">
                        <img src="Img/nobody.jpg">
                        <div class="team-content">
                            <h3>Rozi</h3>
                            <div class="border-team"></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                            <div class="social-icons">
                                <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                                <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 team-box">
                    <div class="team-img thumbnail">
                        <img src="Img/nobody.jpg">
                        <div class="team-content">
                            <h3>Anggi</h3>
                            <div class="border-team"></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                            <div class="social-icons">
                                <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                                <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 team-box ">
                    <div class="team-img thumbnail">
                        <img src="Img/nobody.jpg">
                        <div class="team-content">
                            <h3>Raden</h3>
                            <div class="border-team"></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                            <div class="social-icons">
                                <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                                <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                                <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </div>
    
</div>
<?php include "footer.php"; ?>
</body>
</html>