<?php 
    include_once 'function/connect.php';    
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>

<body>
    <!-- PHP Code naviagasi user -->
    <?php include_once "navigation_user.php"; ?>
    <?php include_once "function/member/member_profile.php"; ?>
    <div class="container">
        <div class="row">
            <?php 
                while ($query_user=mysql_fetch_array($sql_user)) {
        
            ?>
                   
            <div class="col-md-4  toppad  pull-right col-md-offset-4 ">
                <a href="edit_profile.php">Edit Profile</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <?php
                                echo($query_user['nama_depan'].' '.$query_user['nama_belakang']);
                            ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                            <!-- <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive">  -->

                            <img src="Photo/user_<?php echo $query_user['id']?>/<?php echo $query_user['photo']?>" name="aboutme" width="120" height="120" class="img-circle"></a> 
                            </div>                           

                            <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                      <dl>
                        <dt>Nama Depan:</dt>
                        <dd>Sheena</dd>
                        <dt>Nama Belakang</dt>
                        <dd>11/12/2013</dd>
                        <dt>Kode Pos</dt>
                        <dd>xxxxx</dd>
                      </dl>
                    </div>-->
                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                        <tr>
                                            <td>Nama Depan:</td>
                                            <td>
                                                <?php
                                                    echo($query_user['nama_depan']);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nama Belakang:</td>
                                            <td>
                                                <?php
                                                    echo($query_user['nama_belakang']);
                                                ?>
                                            </td>
                                        </tr>

                                       
                                            <tr>
                                                <td>Home Address</td>
                                                <td> 
                                                    <?php
                                                        echo($query_user['alamat']);
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td> 
                                                    <?php
                                                        echo($query_user['email']);
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Virtual Account</td>
                                                <td> 
                                                    <?php
                                                        echo($query_user['no_virtual_account']);
                                                    ?>
                                                </td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
    <?php include "footer.php"; ?>
</body>
</html>
